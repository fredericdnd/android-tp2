package com.fredericdinand.tp2.data.services;

import android.os.Handler;

import com.fredericdinand.tp2.data.models.Card;

import java.util.ArrayList;
import java.util.Collections;

public class CardService {
    public ArrayList<Card> cards = new ArrayList<>();
    Boolean lock = false;
    String firstCardId;
    String secondCardId;
    Boolean enabled = true;
    public ArrayList<String> rightIds = new ArrayList<>();

    private CardService() {
        setupCards();
    }

    private static CardService INSTANCE = new CardService();

    public static CardService getInstance() {
        return INSTANCE;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public void reset() {
        lock = false;
        cards.clear();
        firstCardId = null;
        secondCardId = null;
        rightIds.clear();

        setupCards();
    }

    private void setupCards() {
        cards.add(new Card("0", "img1"));
        cards.add(new Card("1", "img2"));
        cards.add(new Card("2", "img3"));
        cards.add(new Card("3", "img1"));
        cards.add(new Card("4", "img2"));
        cards.add(new Card("5", "img3"));

        Collections.shuffle(cards);
    }

    public void onCardTap(Card card, Runnable notifyChange, Runnable showSuccess) {
        if (!enabled || lock || secondCardId != null || rightIds.contains(card.getImageSrc())) {
            return;
        }

        Boolean mustReturnAll = false;

        if (card.getReverted()) {
            card.setReverted(false);

            firstCardId = null;
            secondCardId = null;
        } else {
            card.setReverted(true);

            if (firstCardId == null) {
                firstCardId = card.getImageSrc();
            } else if (secondCardId == null) {
                lock = true;

                secondCardId = card.getImageSrc();

                if (firstCardId == secondCardId) {
                    rightIds.add(firstCardId);
                    rightIds.add(secondCardId);
                }

                if (rightIds.size() == cards.size()) {
                    notifyChange.run();

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            reset();
                            showSuccess.run();
                        }
                    }, 1000);
                    return;
                }

                firstCardId = null;
                secondCardId = null;

                mustReturnAll = true;
            }
        }

        notifyChange.run();

        if (mustReturnAll) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    revertAll();
                    notifyChange.run();
                    lock = false;
                }
            }, 1000);
        }
    }

    private void revertAll() {
        cards.forEach(card -> {
            if (rightIds.contains(card.getImageSrc())) {
                return;
            }

            card.setReverted(false);
        });
    }
}
