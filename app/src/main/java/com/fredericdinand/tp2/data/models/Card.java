package com.fredericdinand.tp2.data.models;

import java.io.Serializable;

public class Card implements Serializable {
    private static final long serialVersionUID = 1L;
    String id;
    String imageSrc;
    Boolean reverted = false;

    public Card(String id, String imageSrc) {
        this.id = id;
        this.imageSrc = imageSrc;
    }

    public String getId() {
        return id;
    }

    public String getImageSrc() {
        return imageSrc;
    }

    public Boolean getReverted() {
        return reverted;
    }

    public void setReverted(Boolean returned) {
        this.reverted = returned;
    }
}
