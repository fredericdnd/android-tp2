package com.fredericdinand.tp2.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.fredericdinand.tp2.R;
import com.fredericdinand.tp2.data.models.Card;

import java.util.ArrayList;

interface Listener {
    void onCardTap (Card card);
}

public class CardsAdapter extends BaseAdapter {
    private Listener listener;

    private ArrayList<Card> cards = new ArrayList<>();
    private Context context;
    LayoutInflater inflater;

    public CardsAdapter(Context c, ArrayList<Card> cards, Listener listener) {
        this.context = c;
        this.cards = cards;
        this.listener = listener;
        inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return cards.size();
    }

    @Override
    public Object getItem(int i) {
        return CardFragment.newInstance(cards.get(i));
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Card card = cards.get(position);

        convertView = inflater.inflate(R.layout.fragment_card, null);

        View finalConvertView = convertView;
        ImageView cardImageView = finalConvertView.findViewById(R.id.card_image_view);
        ImageView itemImageView = finalConvertView.findViewById(R.id.item_image_view);

        int resId = context.getResources().getIdentifier(card.getImageSrc(), "drawable", context.getPackageName());
        itemImageView.setImageResource(resId);

        if (card.getReverted()) {
            cardImageView.setVisibility(View.INVISIBLE);
            itemImageView.setVisibility(View.VISIBLE);
        } else {
            cardImageView.setVisibility(View.VISIBLE);
            itemImageView.setVisibility(View.INVISIBLE);
        }

        finalConvertView.setOnClickListener(v -> {
            listener.onCardTap(card);
        });

        return finalConvertView;
    }
}
