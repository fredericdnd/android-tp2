package com.fredericdinand.tp2.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageButton;

import com.fredericdinand.tp2.R;

public class SuccessActivity extends AppCompatActivity {
    ImageButton restart_button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_success);

        restart_button = findViewById(R.id.restart_button);

        restart_button.setOnClickListener((v) -> {
            this.finish();
        });
    }
}