package com.fredericdinand.tp2.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.fredericdinand.tp2.R;
import com.fredericdinand.tp2.data.models.Card;
import com.fredericdinand.tp2.data.services.BackgroundSoundService;
import com.fredericdinand.tp2.data.services.CardService;

public class MainActivity extends AppCompatActivity implements PopupMenu.OnMenuItemClickListener {
    GridView cardsContainer;
    CardService cardService;
    CardsAdapter adapter;
    Intent backgroundSountIntent;
    Button soundButton;
    Boolean soundOn = true;
    Button raceButton;
    TextView timerTextView;
    CountDownTimer timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cardService = CardService.getInstance();

        cardsContainer = findViewById(R.id.cards_container);

        playBackgroundSound();

        soundButton = findViewById(R.id.sound_button);

        soundButton.setOnClickListener(v -> {
            if (soundOn) {
                soundButton.setText(R.string.sound_on);
                stopBackgroundSound();
            } else {
                soundButton.setText(R.string.sound_off);
                playBackgroundSound();
            }

            soundOn = !soundOn;
        });

        raceButton = findViewById(R.id.race_button);
        raceButton.setOnClickListener(v -> {
            PopupMenu menu = new PopupMenu(this, raceButton);

            menu.getMenu().add(0, 0, 0, R.string.easy);
            menu.getMenu().add(0, 1, 1, R.string.medium);
            menu.getMenu().add(0, 2, 2, R.string.hard);
            menu.show();

            menu.setOnMenuItemClickListener(this);
        });

        timerTextView = findViewById(R.id.timer_text_view);
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        Log.d("ID", String.valueOf(item.getItemId()));
        if (item.getItemId() == 0) {
            startRace(20000);
        } else if (item.getItemId() == 1) {
            startRace(15000);
        } else if (item.getItemId() == 2) {
            startRace(10000);
        }

        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        initGame();
    }

    private void initGame() {
        cardService.reset();
        cardsContainer.setAdapter(null);
        timerTextView.setVisibility(View.GONE);
        raceButton.setVisibility(View.VISIBLE);
        setupAdapter();
    }

    private void setupAdapter() {
        adapter = new CardsAdapter(this, cardService.cards, new Listener() {
            @Override
            public void onCardTap(Card card) {
                cardService.onCardTap(card, new Runnable() {
                    // Notify change

                    @Override
                    public void run() {
                        adapter.notifyDataSetChanged();
                    }
                }, new Runnable() {
                    // Show success

                    @Override
                    public void run() {
                        timer.cancel();
                        Intent intent = new Intent(MainActivity.this, SuccessActivity.class);
                        startActivity(intent);
                    }
                });
            }
        });

        cardsContainer.setAdapter(adapter);
    }

    private void playBackgroundSound() {
        backgroundSountIntent = new Intent(MainActivity.this, BackgroundSoundService.class);
        startService(backgroundSountIntent);
    }

    private void stopBackgroundSound() {
        stopService(backgroundSountIntent);
    }

    private void startRace(int countdown) {
        timerTextView.setVisibility(View.VISIBLE);
        cardService.setEnabled(false);
        raceButton.setVisibility(View.GONE);

        new CountDownTimer(3000, 1000) {
            public void onTick(long millisUntilFinished) {
                cardsContainer.setClickable(true);
                timerTextView.setText(String.valueOf(millisUntilFinished / 1000 + 1));
            }

            public void onFinish() {
                timer = new CountDownTimer(countdown, 1) {
                    public void onTick(long millisUntilFinished) {
                        cardService.setEnabled(true);
                        timerTextView.setText(String.valueOf(millisUntilFinished / 1000 + 1));
                    }

                    public void onFinish() {
                        if (cardService.rightIds.size() == cardService.cards.size()) {
                            Intent intent = new Intent(MainActivity.this, SuccessActivity.class);
                            startActivity(intent);
                        } else {
                            Intent intent = new Intent(MainActivity.this, FailedActivity.class);
                            startActivity(intent);
                        }
                    }

                }.start();
            }

        }.start();
    }
}